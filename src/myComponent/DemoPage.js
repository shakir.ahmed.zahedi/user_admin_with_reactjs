import React from "react";
import {Box, Grid, makeStyles} from "@material-ui/core";
import {Link} from "react-router-dom";
import LogoUser from "../img/logoUsers.png";


function DemoPage(props) {
    const useStyles = makeStyles({});
    const classes = useStyles();

    return (
        <>
            <Box
                textAlign="center"
                fontWeight="fontWeightBold"
                fontSize="h3.fontSize"
                color="black"
                p={2}
                style={{minHeight: '25vh'}}>

                Welcome To {props.name} Page
                <Box
                    textAlign="center"
                    fontWeight="fontWeightBold"
                    fontSize="h6.fontSize"
                    color="black">

                    In this page we can able to add new {props.name}, delete {props.name},
                    <br/>edit {props.name} and see all the {props.name}.
                </Box>

            </Box>
            <Grid container align="center" justify="center" alignItems="center">
                <Grid Item xs={12} md={6}>
                    <Link to={props.add} style={{textDecoration: 'none'}}>

                        <Box
                            boxShadow={3}
                            mx="auto"
                            py='auto'
                            m={4}
                            p={2}
                            color="black"
                            fontFamily="Pacifico"
                            textAlign="center"
                            fontWeight="fontWeightBold"
                            fontSize="h5.fontSize"
                            style={{width: '15rem', height: '5rem'}}>

                            <br/>
                            Add {props.name}
                        </Box>

                    </Link>
                </Grid>

                <Grid Item xs={12} md={6}>
                    <Link to={props.all} style={{textDecoration: 'none'}}>
                        <Box
                            boxShadow={3}
                            m={4}
                            p={2}
                            color="black"
                            fontFamily="Pacifico"
                            textAlign="center"
                            fontWeight="fontWeightBold"
                            fontSize="h5.fontSize"
                            style={{width: '15rem', height: '5rem'}}>
                            <br/>
                            {props.sub}{props.name}{props.s}
                        </Box>
                    </Link>

                </Grid>
                <img src={LogoUser} className={classes.image}/>
            </Grid>
        </>
    );
}
export default DemoPage;
