import React from "react";
import {Grid} from "@material-ui/core";
import { makeStyles} from "@material-ui/core";
import Logo from '../img/logo.png';
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import {Link} from "react-router-dom";


function Header() {

    const useStyles = makeStyles({

        main: {
            backgroundColor: '#1a867f',
            minHeight: '10vh',
            width: '100',
            paddingTop: '5px',
        },
        image: {
            height: '80px',
            width: '100px',
            paddingLeft: '30px',

        },
        nav: {
            display: 'flex',
        },
        listItem: {
            margin: '1px',
            textAlign: 'center',
            borderRadius: '.5',
            '&:hover': {
                backgroundColor: 'red'
            },

        },
        listText: {
            fontWeight: 'bold',
            fontSize: '1rem',
            color: 'white',
            textDecoration: 'none',
        }
    });
    const classes = useStyles();
    return (
// this is for main css
        <div className={classes.main}>
            <Grid container>
                <Grid Item xs={12} md={4}>
                    <img src={Logo} className={classes.image}/>
                </Grid>
                <Grid Item xs={12} md={7} style={{padding: '5px'}}>
                    <List component="nav" aria-label="main mailbox folders" className={classes.nav}>
                        <ListItem className={classes.listItem}>
                            <Link to="/" className={classes.listText}>
                                HOME
                            </Link>

                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/User" className={classes.listText}>
                                USER
                            </Link>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/right" className={classes.listText}>
                                RIGHT
                            </Link>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/domain" className={classes.listText}>
                                DOMAIN
                            </Link>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <Link to="/RightUser" className={classes.listText}>
                             USER RIGHTS
                            </Link>
                        </ListItem>

                    </List>
                </Grid>

            </Grid>

        </div>
    );
}

export default Header;
