import React, {useEffect, useState} from 'react'
import {useStayl,makeStyles} from "@material-ui/core/"
import ListCard from "./ListCard";
import {InputBase, TablePagination} from "@material-ui/core"
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import UserApi from "./../api/UserApi"
import DomainApi from "../api/DomainApi";
import RightApi from "../api/RightApi";
import SearchIcon from "@material-ui/icons/Search";
import {Link} from "react-router-dom";
import Button from "@material-ui/core/Button";


const useStyles = makeStyles((theme) => ({
    myDiv:{
        minHeight:'63vh',

    },
    title:{
        fontSize:'32px',
        marginTop:'20px',
        fontWeight:'bold',

    },
    card:{
        margin:"24px",
        boxShadow: ' 0 5px 10px 0 rgba(0, 0, 0, 0.2)',

    },

    search: {
        float:"right",
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: "blueGrey" ,
        },
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(5),
            width: 'auto',
            boxShadow: ' 0 5px 10px 0 rgba(0, 0, 0, 0.2)'
        },

    searchIcon: {
        padding: theme.spacing(0,2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft:"950px",
    },
    inputRoot: {
        color: 'inherit',
        backgroundColor: "blueGrey",
        boxShadow: ' 2px 5px 10px 0 rgba(3, 4, 4, 0.2)',
        marginLeft:"950px",
    },
    inputInput: {
        padding: theme.spacing( 1, 4),
        backgroundColor: "blueGrey",
        // vertical padding + font size from searchIcon
        paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
        transition: theme.transitions.create('blueGrey'),
        width: '50%',
        [theme.breakpoints.up('sm')]: {
            width: '12ch',
            '&:focus': {
                width: '20ch',
            },
        },
    },
}));






function ListPage(props) {
    const classes = useStyles();
    const {name} =props;
    const [domains, setDomains] = React.useState([]);
    const [rights, setRights] = React.useState([]);
    const [users,setUsers]=React.useState([]);
    const [loading,setLoading]=React.useState('false');
    const [count,setCount]=React.useState(0);
    const [SearchUser, SetSearch] = React.useState("");
    const [SearchResults, SetSearchResults] = React.useState([]);
    const pages=[5,10,15];
    const [page, setPage]=useState(0);
    const [rowsPerPage,setRowsPerPage]=useState(pages[page]);

    const handleChange = event => {
        SetSearch(event.target.value);
        console.log(SearchUser);
    }


    const deleteClick = (user) => {

        /*if (user.title === 'User') {
            console.log(user);
            UserApi.deleteUser(user.user.id).then(res => {
                alert("Want to delete");

            });
        } else if (user.title === 'Right') {
            RightApi.deleteRight(user.user.id).then(res => {
                alert("Want to delete");

            });
        }
        if (user.title === 'Domain') {
            DomainApi.deleteDomain(user.user.id).then(res => {
                console.log(res);
                alert("Want to delete domain");

            });
        }*/

    }
    const filterUsers =(obj)=> obj.filter(person =>{
        return person.username.toLowerCase().indexOf(SearchUser.toLowerCase())!==-1;
        }

    );
    const filterRights =(obj)=> obj.filter(person =>{
            return person.key.toLowerCase().indexOf(SearchUser.toLowerCase())!==-1;
        }

    );
    const filterDomains =(obj)=> obj.filter(person =>{
            return person.domain.toLowerCase().indexOf(SearchUser.toLowerCase())!==-1;
        }

    );

    useEffect(async () => {

        if (name === 'User')
            UserApi.getAll().then(res => {
                setLoading('true');
                setUsers(res);
                setCount(users.length)
                if(SearchUser!=="")
                    setUsers(filterUsers(users));
            });

        else if (name === 'Domain')
            DomainApi.getAll().then(res => {
                setLoading('true');
                setDomains(res);
                setCount(domains.length)
                //console.log(domains);
                if(SearchUser!=="")
                    setDomains(filterDomains(domains));

            });
        else if (name === 'Right')
            RightApi.getAll().then(res => {
                setLoading('true');
                setRights(res);
                setCount(rights.length)
                if(SearchUser!=="")
                    setRights(filterRights(rights));

            });

        //SetSearchResults(results);
    //console.log(SearchResults)
    }, [SearchUser,deleteClick]);

    function handleChangePage(e,newPage) {
        setPage(newPage);

    }

    function handleChangeRowsPerPage(e) {
        setRowsPerPage(parseInt(e.target.value,10));
        setPage(0);

    }
    const listAfterSelectRowsPerPage=()=>{
        if(name === 'User'){
           // setCount(users.length)
            return users.slice(page*rowsPerPage, (page+1)*rowsPerPage);
        }
        else if(name === 'Domain'){
           // setCount(domains.length)
            return domains.slice(page*rowsPerPage, (page+1)*rowsPerPage);
        }
        else if(name === 'Right'){
            //setCount(rights.length)
            return rights.slice(page*rowsPerPage, (page+1)*rowsPerPage);
        }
    }

    if(loading==='false'){
        return <div>Loading....</div>
    } else {
        return (

            <div className={classes.myDiv}>
                <Typography align="center" className={classes.title}>
                    {props.name} List
                </Typography>
                <div className={classes.search} style={{marginRight:"30px"}}>
                    <div className={classes.searchIcon}>
                    <SearchIcon/>
                    </div>
                    <InputBase
                        type="text"
                        placeholder="Search..."
                        defaultValue=""
                        onChange={handleChange}
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                    />
                </div>

                <Button style={{marginLeft:'7ch'}} variant="contained" color="primary">
                    {
                        name === 'User'?
                            (<Link to={'/User'} style={{textDecoration:'none', color:'white'}}>Back</Link>):""
                    }
                    {
                            name === 'Right'?
                            (<Link to={'/Right'} style={{textDecoration:'none', color:'white'}}>Back</Link>):""
                    }
                    {
                        name === 'Domain'?
                            (<Link to={'/Domain'} style={{textDecoration:'none', color:'white'}}>Back</Link>):""
                    }
                </Button>
                <Card className={classes.card}>

                    <TableContainer>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align='center'> {props.name} Name</TableCell>
                                    <TableCell align="right">Edit</TableCell>
                                    <TableCell align="right">Delete</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {name === 'User' ?
                                listAfterSelectRowsPerPage().map((user) => (
                                    <ListCard  key={user.id} User={user} title={props.name}
                                              />
                                    )) : ""}
                                {name === 'Domain' ?
                                    listAfterSelectRowsPerPage().map((domain) => (
                                        <ListCard key={domain.id} User={domain} title={props.name}

                                        />

                                    )) : ""}
                                {name === 'Right' ?
                                    listAfterSelectRowsPerPage().map((right) => (
                                        <ListCard key={right.id} User={right} title={props.name}
                                                  />

                                    )) : ""}
                            </TableBody>

                        </Table>
                    </TableContainer>
                    <TablePagination
                        component="div"
                        rowsPerPageOptions={pages}
                        rowsPerPage={rowsPerPage}
                        page={page}
                        count={count}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}

                    />
                </Card>
            </div>

        )
    }

}

export default ListPage;

