import React from "react";
import {Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import Typography from "@material-ui/core/Typography";

function Footer() {

    const useStyles = makeStyles({

        main: {
            backgroundColor: '#1a867f',
            minHeight: '10vh',
            width: '100',
            paddingTop: '5px',
        },
        logoText: {
            paddingTop: '70px',
            paddingLeft: "30px",
            textAlign: 'left',
            color: 'white',
            fontWeight: 'bold',
            fontSize: '1.5rem'
        },
        subTitleText: {
            paddingLeft: "30px",
            textAlign: 'left',
            color: 'white',
            fontWeight: 'bold',
            fontSize: '.8rem'
        },
        listItem: {
            margin: '10px',
            textAlign: 'center',
            height: '30px',
            width: '100px',
            '&:hover': {
                backgroundColor: 'red'
            }
        },
        listText: {
            fontSize: '1rem',
            color: 'white',
        }


    });
    const classes = useStyles();
    return (
// this is for main css
        <div className={classes.main}>
            <Grid container>
                <Grid item xs={12} md={6}>
                    <Typography className={classes.logoText}>
                        Stockholm Group
                    </Typography>
                    <Typography className={classes.subTitleText}>
                        @ 2020 All rights reserve
                    </Typography>

                </Grid>
                <Grid item xs={6} md={3}>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem className={classes.listItem}>
                            <ListItemText classes={{primary: classes.listText}} primary="Home"/>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText classes={{primary: classes.listText}} primary="Contact"/>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText classes={{primary: classes.listText}} primary="About Us"/>
                        </ListItem>

                    </List>

                </Grid>
                <Grid item xs={16} md={3} style={{padding: '5px'}}>
                    <List component="nav" aria-label="main mailbox folders">
                        <ListItem className={classes.listItem}>
                            <ListItemText classes={{primary: classes.listText}} primary="User"/>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText classes={{primary: classes.listText}} primary="Right"/>
                        </ListItem>
                        <ListItem className={classes.listItem}>
                            <ListItemText classes={{primary: classes.listText}} primary="Domain"/>
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
        </div>
    );
}

export default Footer;