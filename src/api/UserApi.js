import React, {useState, useEffect} from 'react'


class UserApi {
    getAll = async () => {
        const response = await fetch('https://usersjava20.sensera.se/users');
        const result = await response.json();
        return result;
        //removeClick={(user)=>deleteClick(user)}
    }
    addUser = async (data = {}) => {
        const response = await fetch('https://usersjava20.sensera.se/users',
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }
    addRightsToUser = async (data = {})=>{
        const response = await fetch('https://usersjava20.sensera.se/users/addRightToUser',
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;

    }

    updateUser = async (id, data = {}) => {
        const response = await fetch('https://usersjava20.sensera.se/users/' + id,
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }

    deleteUser = async (id) => {
        const response = await fetch('https://usersjava20.sensera.se/users/'+id,
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json"},
            });
        const result = await response;
        return result;
    }

    removeRightFromUser =async (data = {})=>{
        const response = await fetch('https://usersjava20.sensera.se/users/removeRightFromUser',
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;

    }


    return;
}

export default new UserApi();
