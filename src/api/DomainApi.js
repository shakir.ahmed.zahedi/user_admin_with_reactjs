import React from 'react'


class DomainApi {
    getAll = async () => {
        const response = await fetch('https://usersjava20.sensera.se/domains',
            {
                method: 'GET',
                headers: {"Content-type": "application/json"}
            });
        const result = await response.json();
        return result;

    }

    addDomain = async (data = {}) => {
        const response = await fetch('https://usersjava20.sensera.se/domains',
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;

    }

    findDomain = async (name) => {
        const response = await fetch('https://usersjava20.sensera.se/domains/find?domain='+name,
            {
                method: 'GET',
                headers: {"Content-type": "application/json"},
            });
        const result = await response.json();
        return result;
    }

    updateDomain = async (id, data = {}) => {
        const response = await fetch('https://usersjava20.sensera.se/domains/' +id,
            {
                method: 'POST',
                headers: {"Content-type": "application/json"},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }

    deleteDomainWithRight = async (id,name) => {
        const response = await fetch('https://usersjava20.sensera.se/domains/' +id+"/"+name,
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json"},
            });
        const result = await response;
        return result;
    }
    deleteDomain = async (id) => {
        const response = await fetch('https://usersjava20.sensera.se/domains/' +id,
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json"},
            });
        const result = await response;
        return result;
    }
    return;
}

export default new DomainApi();

